% This file is included by notes.tex and slides.tex

\usepackage[english,british]{babel}
\usepackage{pgf,pgfarrows,pgfshade}
\usepackage{url}

\newcommand\image[2][]{\mode<beamer>{\includegraphics[#1]{#2}}}

\newcommand\notes[1]{\mode<article>{#1}}

\DeclareUrlCommand\email{%
  \def\UrlLeft{<}%
  \def\UrlRight{>}%
  \urlstyle{tt}%
}

% end of preamble

\title{
  Making a Mandelbrot Movie
}
\subtitle{
  \url{http://dotat.at/prog/mandelbrot/}
}

\author[Tony~Finch]{
  Tony~Finch \email{dot@dotat.at}
}

\institute{%
  \large%
  \raisebox{2.75em}{%
    \includegraphics[height=5em,angle=180]{icon.png}%
  }%
  \hspace{0.5em}%
  Cambridge Geek Night 6%
  \hspace{0.5em}%
  \raisebox{-2.25em}{%
    \includegraphics[height=5em]{icon.png}%
  }%
}

\date{Monday 8 November 2010}

%\titlegraphic{%
%}

\begin{document}

\frame{\maketitle}

\notes{
  \begin{abstract}
    How I made a movie that zooms from $2^2$ to $2^{-48}$.
  \end{abstract}
}

%

\section{Intro}

\begin{frame}
  \frametitle{Beno\^it Mandelbrot, 1924 -- 2010}
  \centering
  \image[width=75mm]{benoit.jpg}
\end{frame}

\notes{
  When Mandelbrot died on the 14th October I thought I could celebrate
  his life and work by trying to do something with Mandelbrot sets that
  I haven't done before.
}

%

\begin{frame}
  \frametitle{Coloured level sets}
  \centering
  \image[width=100mm]{eg-step.png}
\end{frame}

\notes{
  When I drew Mandelbrot sets as a teenager they looked a bit like
  this. The blocky colouring and noisy fine detail are not as pretty
  as some of the professional fractal art I had seen.
}

%

\begin{frame}
  \frametitle{Renormalized iteration count}
  \centering
  \image[width=100mm]{eg-fast.png}
\end{frame}

\notes{
  So the first thing I wanted to do is draw Mandelbrot sets where the
  points outside the set are smoothly coloured, like this.
}

%

\newcommand\abs[1]{\left|{#1}\right|}

\begin{frame}[fragile]
  \frametitle{Renormalized iteration count}
  {\tt /* You are not expected to understand this. */}
  \begin{columns}
    \begin{column}{5cm}
      \tt
\begin{verbatim}
while (n++ < max &&
        x2+y2 < inf) {
    y = 2*x*y + b;
    x = x2-y2 + a;
    y2 = y*y;
    x2 = x*x;
}
nu = n - log(log(x2+y2)/2)
          / log(2);
\end{verbatim}
    \end{column}
    \begin{column}{5cm}
      \begin{align*}
        \\
        z_0 &= 0 \\
        z_n &= z_{n-1}^2 + c \\
        \\
        \\
        \nu &= n - \log_2(\log{\abs{z_n}})
      \end{align*}
    \end{column}
  \end{columns}
\end{frame}

\notes{
  It turns out to be quite simple to code and quick to execute. The
  inner loop is exactly the same as before. You just need to take a
  couple of logarithms when you find the point is outside the set
  \cite{linas-renormalize}.
}

%

\begin{frame}
  \frametitle{Log normalized iteration count}
  \centering
  \image[width=100mm]{eg-fade.png}
\end{frame}

\notes{
  You can get rid of the visual noise close to the Mandelbrot set
  itself by taking the log of the normalized iteration count. This is
  exactly the same view of the Manelbrot set as the last two I showed
  you, just coloured more nicely.
}

%

\begin{frame}
  \frametitle{Tentacles!}
  \centering
  \image[width=100mm]{pretty.png}
\end{frame}

\notes{
  Now you know how to make stunning pictures like this.
}

%

\begin{frame}
  \frametitle{Demo}
  \mode<beamer>{\Large}
  \begin{columns}
    \begin{column}{6cm}
      But this talk is supposed to be about {\em moving} pictures!
      \begin{itemize}
      \item Here's what I made ...
      \end{itemize}
    \end{column}
    \begin{column}{4cm}
      \image[width=5cm]{movie.jpg}
    \end{column}
  \end{columns}
\end{frame}

\notes{
  Next I wanted to make moving pictures of the Mandelbrot set, so I'm
  going to explain how while showing you what I made.
}

%

\notes{

\section{Making a movie}

It turns out to be pretty easy to turn a stack of images into a movie.

{\tt ffmpeg} will encode a directory containing serial-numbered {\tt
  ppm} files into a film, though you have to Google a bit to find out
how.

Make sure that {\tt ffmpeg} uses {\tt x264} as the codec, otherwise
you will suffer from horrible compression artefacts and bloaty file
sizes.

The reason for using {\tt ppm} files is they are completely trivial to
write, even if they are a bit big. (I have 22GB working files for this
movie.)

I have separated the Mandelbrot generation into two stages.

The first stage just does the Mandelbrot calculation and emits an
array of normalized iteration counts to a file.

The second stage converts the iteration count array into a {\tt ppm}
file.

I can change the colouring or experiment with other effects without
having to re-calculate every frame from scratch.

One thing I haven't yet worked out is how to get rid of the flickering
alias effects - Sorry!

The Mandelbrot calculation itself is specially designed for deep zooms.

The program re-scans the points that haven't yet escaped, going
exponentially deeper and deeper until the number of pixels that
escapes during a scan is below a threshold.

There is also a minimum number of pixels that must escape before it
stops scanning, in case the whole image is deeper than the starting
depth.

Near the end of the movie each frame requires millions of iterations
per pixel, and takes many minutes to calculate.

I used GNU {\tt parallel} to calculate multiple frames concurrently.

This zoom goes as deep as possible using double precision floating
point arithmetic.

We start running out of resolution at a pixel size of $2^{-50}$.

When the total depth of a zoom is $2^{53}$ then the startimg image has
been blown up to a lightyear across, because it turns out a lightyear
is about $2^{53}$ metres!

}

%

\section{Conclusion}

\begin{frame}
  {
    \centering
    \image[width=6.25cm]{thats-all.jpg}
  }
  \mode<beamer>{\Large}

  \begin{itemize}
  \item
    \url{http://dotat.at/prog/mandelbrot/}
  \item
    \url{http://fanf.livejournal.com/}
  \item
    \url{http://twitter.com/fanf}
  \end{itemize}
\end{frame}

\notes{
  The code, slides, and notes will be available on my web site.
  You might also have a look at my blog and twitter feed.
}

%

\bibliography{talk}
\bibliographystyle{plain}

\end{document}

% eof
